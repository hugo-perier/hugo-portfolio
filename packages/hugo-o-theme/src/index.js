import Root from "./components/app"

const HugoPortfolio = {
  name: "hugo-o-theme",
  roots: {
    theme: Root,
  },
  state: {
    theme: {},
  },
  actions: {
    theme: {},
  },
}

export default HugoPortfolio