import React from "react"
import { Global, css, connect } from "frontity"
import { AnimatePresence, motion } from "framer-motion"

import VG5000Eot from "../fonts/VG5000-Regular_web.eot"
import VG5000Ttf from "../fonts/VG5000-Regular_web.ttf"
import VG5000Woff from "../fonts/VG5000-Regular_web.woff"
import VG5000Woff2 from "../fonts/VG5000-Regular_web.woff2"

import Link from "@frontity/components/link"
import Switch from "@frontity/components/switch"
import Loading from "./Organisms/Loading"
import Page from "./Templates/Page"
import Post from "./Templates/Post"
import List from "./Templates/List"
import PageTransition from "./Utils/PageTransition"


const Root = ({ state }) => {
    const data = state.source.get(state.router.link);
    const location = state.router;

    return (
        <>
            <Global styles={globalStyles} />

            <h1>Moi c'est Hugo</h1>
            <p>Current URL: {state.router.link}</p>
            <nav>
                <Link link="/">Home</Link>
                <br />
                <Link link="/page/2">More posts</Link>
                <br />
                <Link link="/about-us">About Us</Link>
            </nav>

            <main>
                <AnimatePresence exitBeforeEnter>
                    <Switch location={location} key={location.link}>
                        <Loading when={data.isFetching} />
                        <PageTransition when={data.isArchive} component={<List />} />
                        <PageTransition when={data.isPost} component={<Post />} />
                        <PageTransition when={data.isPage} component={<Page />} />
                    </Switch>
                </AnimatePresence>
            </main>
        </>
    )
}

const globalStyles = css`
    :root {
        --brick: #bf322b;
        --poppy: #eb4948;
        --abricot: #ffd064;
        --peach: #ffda95;
        --shell: #fcf0db;
        --brown: #937765;
        --gray: #62564e;
        --gray-dark: #312822;
        --dark: #000;
        --light: #fff;
    }

    @font-face{
        font-family: 'VG5000';
        src: url("${VG5000Eot}");
        src: url("${VG5000Woff}") format('woff'),
             url("${VG5000Woff2}") format('woff2'),
             url("${VG5000Ttf}") format('truetype');
        font-weight: normal;
        font-style: normal;
    }

    html {
        scroll-behavior: smooth;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    body {
        margin: 0;

        > div{
            background-color: orange;
            margin: 3em auto;
            width: 50%;
        }
    }

    *{
        font-family: 'VG5000';
        font-weight: normal;
    }

    img{
        max-width: 100%;
    }
`;

export default connect(Root)