import React from "react"
import { connect, styled } from "frontity"
import { motion } from "framer-motion"

const Loading = () => {

    return (
        <LoadingContainer>
            <h1>Coucou</h1>
        </LoadingContainer>
    )
}

const LoadingContainer = styled(motion.div)`
    align-items: center;
    background-color: var(--brick);
    bottom: 0;
    display: flex;
    height: 100%;
    justify-content: center;
    left: 0;
    position: fixed;
    width: 100%;
`;

export default connect(Loading)