import React from "react"
import { connect, styled } from "frontity"

const Post = () => {
    return (
        <>
            <div>
                This is a Post
            </div>
            <img src={"https://picsum.photos/100/100"} alt="test" />
        </>
    )
}

export default connect(Post)