import React from "react"
import { connect } from "frontity"

const List = () => {

    return (
        <>
            <h1>This is a list</h1>
            <img src={"https://picsum.photos/210/3000"} alt="test" />
            <img src={"https://picsum.photos/2000/3000"} alt="test" />
            <img src={"https://picsum.photos/2040/3000"} alt="test" />
        </>
    )
}

export default connect(List)